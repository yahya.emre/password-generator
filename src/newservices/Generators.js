import _ from 'lodash';
export function createNewDictionary(words, settings) {
  const tempArray = [];
  words.map((word) => {
    tempArray.push(word);
  });
  for (let i = 0; i < 5; i++) {
    modifyWord(tempArray, settings, i).forEach((word) => {
      tempArray.push(word);
    });
  }

  return _.uniq(tempArray);
}

// Takes a list of words, and capitilize each word.
//@param {Array} words - list of words
export function listUpperCase(words) {
  return words.map((word) => word[0].toUpperCase() + word.slice(1));
}

// Take a list of words, and change some letters of the words with special characters with a probability for each letter.
// For each letter, there is a 20% probability that it will be replaced with special character
//@param {Array} words - list of words
export function modifyWord(words, settings, forNumber) {
  return words.map((word) => {
    let newWord = word;
    if (settings.isCapitalized) {
      newWord = word[0].toUpperCase() + word.slice(1);
    }
    if (settings.removeRandomLetters && forNumber === 0) {
      newWord = removeRandomLetter(newWord);
    }
    if (settings.addRandomLetter && forNumber === 0) {
      newWord = addRandomLetter(newWord);
    }
    if (settings.isSpecialChars) {
      newWord = changeCharactersWithProbability(newWord);
    }

    return newWord;
  });
}
export function removeRandomLetter(word) {
  const letters = word.split('');
  const randomIndex = Math.floor(Math.random() * letters.length + 1);
  letters.splice(randomIndex, 1);
  return letters.join('');
}
export function addRandomLetter(word) {
  const letters = word.split('');
  const randomIndex = Math.floor(Math.random() * letters.length);
  letters.splice(randomIndex, 0, word[randomIndex]);
  //   console.log(letters.join(''));
  //   console.log(letters);
  return letters.join('');
}

function changeCharactersWithProbability(word) {
  const substitudes = [
    [/a/, '@'],
    [/e/, '€'],
    [/E/, '3'],
    [/s/, '$'],
    [/B/, '8'],
    [/i/, '!'],
    [/o/, '0'],
    [/I/, '1'],
    [/O/, '0'],
    [/S/, '5'],
  ];
  let newWord = word;
  for (let substitude of substitudes) {
    newWord =
      Math.floor(Math.random() * 100) < 20
        ? word.replace(substitude[0], substitude[1])
        : word;
  }
  return newWord;
}
