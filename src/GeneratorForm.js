import React, { useState } from 'react';
import 'antd/dist/antd.css';
import './index.css';
import { Button, Form, Input, InputNumber, Switch, Typography } from 'antd';
import { generatePassphrase } from './services/generate';
const { Title } = Typography;

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
/* eslint-disable no-template-curly-in-string */

const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};
/* eslint-enable no-template-curly-in-string */
function calculateCrackTime(entropy) {
  const NUMBER_OF_GUESSES = 1000000;
  const SECONDS_IN_YEAR = 31556952;
  const seconds = Math.pow(2, entropy) / NUMBER_OF_GUESSES;
  return seconds / SECONDS_IN_YEAR;
}

export const GeneratorForm = () => {
  const [generatedPassword, setGeneratedPassword] = useState('');
  const [entropy, setEntropy] = useState(0);
  const [crackTime, setCrackTime] = useState(0);
  const [dictionaryLength, setDictionaryLength] = useState(0);
  const [average, setAverage] = useState(0);

  const onFinish = (values) => {
    const { password, entropy, totalSize, averageWordLength } =
      generatePassphrase(values.settings);
    setDictionaryLength(totalSize);
    setAverage(averageWordLength);
    setGeneratedPassword(password.join(''));
    setEntropy(Math.round(entropy));
    setCrackTime(
      calculateCrackTime(Math.round(entropy)).toLocaleString('en-US', {
        maximumFractionDigits: 2,
      })
    );
  };

  return (
    <>
      <Form
        {...layout}
        name="nest-messages"
        onFinish={onFinish}
        validateMessages={validateMessages}
      >
        <Form.Item
          name={['settings', 'numberOfWords']}
          label="Number of Words"
          rules={[
            {
              required: true,
              type: 'number',
              min: 3,
              max: 10,
            },
          ]}
        >
          <InputNumber />
        </Form.Item>
        <Form.Item
          name={['settings', 'isCapitalized']}
          label="Is Capitalized? "
        >
          <Switch />
        </Form.Item>
        <Form.Item
          name={['settings', 'isSpecialChars']}
          label="Special Characters? "
        >
          <Switch />
        </Form.Item>
        <Form.Item
          name={['settings', 'addNumbers']}
          label="Add Random Numbers at the end? "
        >
          <Switch />
        </Form.Item>
        <Form.Item
          name={['settings', 'removeRandomLetters']}
          label="Should remove random letters? "
        >
          <Switch />
        </Form.Item>
        <Form.Item
          name={['settings', 'addRandomLetter']}
          label="Should add random letters? "
        >
          <Switch />
        </Form.Item>
        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
          <Button type="primary" htmlType="submit">
            Generate Password
          </Button>
        </Form.Item>
        <Form.Item label="Password ">
          <Input value={generatedPassword} />
        </Form.Item>
        <Form.Item label="Entropy ">
          <Input value={entropy} />
        </Form.Item>
        <Form.Item label="Required Years to crack password with 1.000.000 try per second (Brute Force) ">
          <Input value={crackTime} />
        </Form.Item>
      </Form>
      <Title level={4}>
        Selecting a word at random from the resulting list has an entropy value
        of {Math.log2(dictionaryLength).toFixed(2)} bits. (log2 of{' '}
        {dictionaryLength.toLocaleString()} ). The average length of each word
        is {average.toFixed(2)} characters. This means that a passphrase
        generated from this list will average an entropy of{' '}
        {(Math.log2(dictionaryLength).toFixed(2) / average).toFixed(2)} bits per
        character, not counting the spaces between words.
      </Title>
    </>
  );
};
