import logo from './logo.svg';
import './App.css';
import { GeneratorForm } from './GeneratorForm';

function App() {
  return <GeneratorForm />;
}

export default App;
