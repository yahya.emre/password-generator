const { changeCharactersWithProbability } = require('./modifier');

export function createDictionary(words, settings) {
  const wordsSet = new Set();
  const tempArray = [];
  words.map((word) => {
    tempArray.push(word);
  });
  for (let i = 0; i < 5; i++) {
    changeCharactersWithProbability(tempArray, settings).forEach((word) => {
      tempArray.push(word);
    });
  }
  tempArray.forEach((word) => {
    wordsSet.add(word);
  });

  return wordsSet;
}

// console.log(createDictionary(adjs).size); // => around 10429 unique words
// console.log(createDictionary(newVerbs).size); // => around 7461 unique words
// console.log(createDictionary(nouns).size); // => around 11532 unique words
// total = 10429 + 7461 + 11532 = 29422
// entropy  with 3 words = e = 3 * log(29422)/log(2) = 44 bits
// with random letter removal dictionary sizes are 589251,131025,600493
// entropy  with 3 words = e = 3 * log(29422)/log(2) =  60 bits

// with random letter removal  and adder dictionary sizes are 2.069.156, 1.098.877, 2.091.418
// entropy  with 3 words = e = 3 * log(5259451)/log(2) =  66 bits
