const { generatePassphrase } = require('./generate');
const adjs = require('../res/adj');
const verbs = require('../res/verbs');
const nouns = require('../res/nouns');

const setting = {
  numberOfWords: 5,
  isCapitalized: true,
  isSpecialChars: true,
  isNumbers: true,
  // number: 1071,
  // wieviele bits sollte ich haben?
  //number at the end
  // just remove a letter, or put extra letter
};
console.log(`adjectives  = ` + adjs.length);
console.log(`verbs length = ` + verbs.length);
console.log(`nouns length = ` + nouns.length);
console.log(generatePassphrase(setting));
