// Takes a list of words, and capitilize each word.
//@param {Array} words - list of words
export function listUpperCase(words) {
  return words.map((word) => word[0].toUpperCase() + word.slice(1));
}

// Take a list of words, and change some letters of the words with special characters with a probability for each letter.
// For each letter, there is a 20% probability that it will be replaced with special character
//@param {Array} words - list of words
export function changeCharactersWithProbability(words, settings) {
  const substitudes = [
    [/a/, '@'],
    [/e/, '€'],
    [/E/, '3'],
    [/s/, '$'],
    [/B/, '8'],
    [/i/, '!'],
    [/o/, '0'],
    [/I/, '1'],
    [/O/, '0'],
    [/S/, '5'],
  ];

  return words.map((word) => {
    let newWord = word;
    if (settings.isCapitalized) {
      newWord = word[0].toUpperCase() + word.slice(1);
    }
    if (settings.isSpecialChars) {
      for (let substitude of substitudes) {
        newWord =
          Math.floor(Math.random() * 100) < 20
            ? newWord.replace(substitude[0], substitude[1])
            : newWord;
      }
    }

    if (settings.removeRandomLetters) {
      newWord = removeRandomLetter(newWord);
    }
    if (settings.addRandomLetter) {
      newWord = addRandomLetter(newWord);
    }

    return newWord;
  });
}
export function removeRandomLetter(word) {
  const letters = word.split('');
  const randomIndex = Math.floor(Math.random() * letters.length);
  letters.splice(randomIndex, 1);
  return letters.join('');
}
export function addRandomLetter(word) {
  const letters = word.split('');
  const randomIndex = Math.floor(Math.random() * letters.length);
  letters.splice(randomIndex, 0, word[randomIndex]);
  return letters.join('');
}
