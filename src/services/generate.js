import { adjs } from '../res/adj';
import { verbs } from '../res/verbs';
import { nouns } from '../res/nouns';
import { createDictionary } from './dictionaryCreator';
import { createNewDictionary } from '../newservices/Generators';

const { capitalize, changeCharactersWithProbability } = require('./modifier');
// // generate password with given number of words and settings
// export function generatePassphrase(settings) {
//   // first noun, then verb,entr then adjective, then noun, and repeat it
//   let passphrase = [];
//   for (let i = 0; i < settings.numberOfWords; i++) {
//     if (i % 3 === 0) {
//       passphrase.push(generateNoun());
//     }entr
//     if (i % 3 === 1) {
//       passphrase.push(generateVerb());
//     }
//     if (i % 3 === 2) {
//       passphrase.push(generateAdjective());
//     }
//   }

//   console.log(adjs.length + getVerbs(verbs).length + nouns.length);
//   const adjDictionary = createDictionary(adjs, settings);
//   const verbDictionary = createDictionary(getVerbs(verbs), settings);
//   const nounDictionary = createDictionary(nouns, settings);
//   let totalSize =
//     adjDictionary.size + verbDictionary.size + nounDictionary.size;

//   if (settings.randomNumber) {
//     totalSize += 9999;
//   }
//   console.log(totalSize);
//   return {
//     password: handleSettings(passphrase, settings),
//     entropy: calculateEntropy(settings.numberOfWords, totalSize),
//     totalSize: totalSize,
//   };
// }

export function generatePassphrase(settings) {
  const adjDictionary = createNewDictionary(adjs, settings);
  const verbDictionary = createNewDictionary(getVerbs(verbs), settings);
  const nounDictionary = createNewDictionary(nouns, settings);
  let passphrase = [];
  for (let i = 0; i < settings.numberOfWords; i++) {
    if (i % 3 === 0) {
      passphrase.push(getRandomWord(nounDictionary));
    }
    if (i % 3 === 1) {
      passphrase.push(getRandomWord(verbDictionary));
    }
    if (i % 3 === 2) {
      passphrase.push(getRandomWord(adjDictionary));
    }
  }

  let totalSize =
    adjDictionary.length + verbDictionary.length + nounDictionary.length;
  const totalWordLength =
    adjDictionary.join('').length +
    verbDictionary.join('').length +
    nounDictionary.join('').length;
  const averageWordLength = totalWordLength / totalSize;

  if (settings.addNumbers) {
    totalSize += 9999;
  }

  console.log(`total size: ${totalSize}`);
  return {
    password: handleSettings(passphrase, settings),
    entropy: calculateEntropy(settings.numberOfWords, totalSize),
    totalSize,
    averageWordLength,
  };
}
function getVerbs(verbs) {
  const newVerbs = [];
  verbs.forEach((verb) => {
    newVerbs.push(verb.present);
    newVerbs.push(verb.past);
  });
  return newVerbs;
}
function calculateEntropy(numberOfWords, totalSize) {
  // entropy  with n words = e = numberOfWords * log2(totalSize) = 44 bits and
  return numberOfWords * Math.log2(totalSize);
}
function handleSettings(passphrase, settings) {
  let newPassphrase = passphrase;

  if (settings.isCapitalized) {
    newPassphrase.map((word) => word[0].toUpperCase() + word.slice(1));
  }
  if (settings.isSpecialChars) {
    newPassphrase = changeCharactersWithProbability(newPassphrase, settings);
  }

  if (settings.addNumbers) {
    const randomNumber = Math.floor(Math.random() * 9999);
    newPassphrase.push(randomNumber);
  }

  return newPassphrase;
}
function getRandomWord(wordsSet) {
  const words = Array.from(wordsSet);
  return words[Math.floor(Math.random() * words.length)];
}
